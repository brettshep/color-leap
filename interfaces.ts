export interface Palette {
  name: string;
  colors: string[];
  id: string;
}

export interface User {
  uid: string;
  email: string;
  likes: string[];
}

export interface ColorData {
  [key: string]: Palette[];
}

export interface Badge {
  src: string;
  date: string;
}

export interface Poster {
  image: string;
  colors: string[];
  bgCol: "#97DBE3";
}
