CDDATA = {
  "2000BC": {
    palettes: [{
        name: "Sarcophagus",
        colors: ["#DCAE61", "#874526", "#9FB7A0", "#7E9C6F", "#4B5C54"],
        id: "2000BC-0"
      },
      {
        name: "Noble Man",
        colors: ["#DAA738", "#9A6619", "#B7C0C0", "#645F52", "#3C2C2D"],
        id: "2000BC-1"
      },
      {
        name: "Jar of Tears",
        colors: ["#8C9DA4", "#C99E69", "#B1542E", "#773118", "#531A06"],
        id: "2000BC-2"
      },
      {
        name: "Blue Servant",
        colors: ["#3893B1", "#93B7CC", "#267DBD", "#013876", "#132434"],
        id: "2000BC-3"
      },
      {
        name: "The Scroll",
        colors: ["#6F7B55", "#AB8D61", "#A47734", "#8F551E", "#682F10"],
        id: "2000BC-4"
      },
      {
        name: "Prophecy",
        colors: ["#134F57", "#B79C7D", "#AF8C62", "#785943", "#5F4C46"],
        id: "2000BC-5"
      },
      {
        name: "Forgotten Gem",
        colors: ["#0EA2BE", "#FAD15B", "#8B9FA5", "#57565A", "#2F2D2F"],
        id: "2000BC-6"
      },
      {
        name: "Royal Face",
        colors: ["#C84930", "#363636", "#D6764F", "#D5C455", "#4494B0"],
        id: "2000BC-7"
      },
      {
        name: "Hidden Tomb",
        colors: ["#CB9527", "#853A1D", "#88783C", "#494E47", "#30302C"],
        id: "2000BC-8"
      },
      {
        name: "Sacrifice",
        colors: ["#930A0E", "#D09D85", "#BE7F66", "#884425", "#07020F"],
        id: "2000BC-9"
      },
      {
        name: "Ocean of Sand",
        colors: ["#DCC5A3", "#D0AD7D", "#B17551", "#995C3F", "#6E3C31"],
        id: "2000BC-10"
      },
      {
        name: "The End",
        colors: ["#BBAB8B", "#8F7961", "#795D49", "#684E38", "#201C19"],
        id: "2000BC-11"
      },
      {
        name: "Wall Writing",
        colors: ["#D0D0CA", "#C9A528", "#9B5226", "#7A3315", "#4C3723"],
        id: "2000BC-12"
      },
      {
        name: "Broken History",
        colors: ["#AEA59C", "#5B6C71", "#3D4635", "#A76C44", "#523213"],
        id: "2000BC-13"
      },
      {
        name: "Clay Life",
        colors: ["#B2B6BF", "#DDCDBE", "#B8A890", "#AD615B", "#614943"],
        id: "2000BC-14"
      }
    ],
    posters: [{
        bgCol: "#454944",
        image: "/assets/posters/2000BC/0.jpg",
        colors: ["#DCAE61", "#874526", "#9FB7A0", "#7E9C6F", "#4B5C54"]
      },
      {
        bgCol: "#E4E1DB",
        image: "/assets/posters/2000BC/1.jpg",
        colors: ["#DAA738", "#9A6619", "#B7C0C0", "#645F52", "#3C2C2D"]
      },
      {
        bgCol: "#8C523A",
        image: "/assets/posters/2000BC/2.jpg",
        colors: ["#8C9DA4", "#C99E69", "#B1542E", "#773118", "#531A06"]
      }
    ]
  },
  "0": {
    palettes: [{
        name: "Ritual",
        colors: ["#EFDEC4", "#F8AB51", "#EE8927", "#151829", "#080705"],
        id: "0-0"
      },
      {
        name: "Stone Fountain",
        colors: ["#F9F4E4", "#EEBB4E", "#AD7432", "#6DBDC4", "#0A203F"],
        id: "0-1"
      },
      {
        name: "Paradise",
        colors: ["#7F7340", "#C6B28D", "#BB8642", "#914532", "#511F20"],
        id: "0-2"
      },
      {
        name: "Mother Earth",
        colors: ["#857E6B", "#D0C1AE", "#C8A15C", "#BA4033", "#8C433A"],
        id: "0-3"
      },
      {
        name: "Four Corners",
        colors: ["#BCBBB7", "#CD9E34", "#622834", "#164072", "#322424"],
        id: "0-4"
      },
      {
        name: "Craftsman",
        colors: ["#655255", "#B48959", "#733D25", "#42150F", "#100606"],
        id: "0-5"
      },
      {
        name: "Resting Bird",
        colors: ["#F8EAD0", "#E1C7A2", "#7A786B", "#4F5146", "#3B372E"],
        id: "0-6"
      },
      {
        name: "Farmer’s Break",
        colors: ["#F8ECD9", "#FEBE8E", "#C87A4B", "#31322C", "#0F1411"],
        id: "0-7"
      },
      {
        name: "The Hero",
        colors: ["#DFE5E3", "#BAB2AD", "#E7B409", "#8C4501", "#3F0D0A"],
        id: "0-8"
      },
      {
        name: "Spoken Word",
        colors: ["#FCCC7A", "#D7A35A", "#9A572D", "#562F20", "#3E2419"],
        id: "0-9"
      },
      {
        name: "Defeating the Giant",
        colors: ["#FFC253", "#DF8601", "#8F3B03", "#701B00", "#020200"],
        id: "0-10"
      },
      {
        name: "Precious Stone",
        colors: ["#E1E1E1", "#D7BF47", "#186F2E", "#0550AB", "#072053"],
        id: "0-11"
      },
      {
        name: "Roaming Donkey",
        colors: ["#8F965F", "#C49624", "#A56333", "#85321F", "#453633"],
        id: "0-12"
      },
      {
        name: "Immortal Angel",
        colors: ["#F7EBD4", "#B98556", "#715C49", "#534A41", "#473025"],
        id: "0-13"
      },
      {
        name: "Garden",
        colors: ["#97151F", "#DFE5E5", "#176C6A", "#013B44", "#212220"],
        id: "0-14"
      }
    ],
    posters: [{
        bgCol: "#3D415A",
        image: "/assets/posters/0/0.jpg",
        colors: ["#EFDEC4", "#F8AB51", "#EE8927", "#151829", "#080705"]
      },
      {
        bgCol: "#707A87",
        image: "/assets/posters/0/1.jpg",
        colors: ["#F9F4E4", "#EEBB4E", "#AD7432", "#6DBDC4", "#0A203F"]
      },
      {
        bgCol: "#624445",
        image: "/assets/posters/0/2.jpg",
        colors: ["#7F7340", "#C6B28D", "#BB8642", "#914532", "#511F20"]
      }
    ]
  },
  "1000": {
    palettes: [{
        name: "Holy Light",
        colors: ["#F4E8B8", "#EEC30E", "#8F0100", "#058187", "#0C4DA7"],
        id: "1000-0"
      },
      {
        name: "Discipline",
        colors: ["#B9B2A3", "#404948", "#D6AF6C", "#B44541", "#632621"],
        id: "1000-1"
      },
      {
        name: "Pages",
        colors: ["#CDA561", "#AB672D", "#CAC2B3", "#989FB1", "#174073"],
        id: "1000-2"
      },
      {
        name: "Taming the Lion",
        colors: ["#F0F1EC", "#C98E43", "#98603A", "#64463C", "#412E2B"],
        id: "1000-3"
      },
      {
        name: "Wisdom",
        colors: ["#949CA3", "#334762", "#B39A67", "#D8AE46", "#B24C2E"],
        id: "1000-4"
      },
      {
        name: "Fine Wine",
        colors: ["#E3C878", "#B15142", "#7E3728", "#52544E", "#363732"],
        id: "1000-5"
      },
      {
        name: "Caged",
        colors: ["#E0DEDF", "#6EC0B8", "#257F6A", "#9B6133", "#664326"],
        id: "1000-6"
      },
      {
        name: "Heaven’s Sky",
        colors: ["#EAC485", "#AA857A", "#936435", "#81241F", "#362A2C"],
        id: "1000-7"
      },
      {
        name: "Seasons",
        colors: ["#E4D4CF", "#8E9B6E", "#E2BA3B", "#BB742B", "#A81D1C"],
        id: "1000-8"
      },
      {
        name: "Harvest",
        colors: ["#CB9C53", "#826E49", "#B04130", "#6C3D35", "#38222F"],
        id: "1000-9"
      },
      {
        name: "Children at Play",
        colors: ["#DAB387", "#B9793B", "#D49C35", "#B23306", "#1F4062"],
        id: "1000-10"
      },
      {
        name: "Mother",
        colors: ["#A45B40", "#D5B698", "#BC8A5C", "#8D603F", "#3F5C75"],
        id: "1000-11"
      },
      {
        name: "Distant Lands",
        colors: ["#777453", "#B15441", "#DAAB6C", "#7D776F", "#745D5A"],
        id: "1000-12"
      },
      {
        name: "Service",
        colors: ["#FEF7EE", "#FEF000", "#FB0002", "#1C82EB", "#190C28"],
        id: "1000-13"
      },
      {
        name: "Invention",
        colors: ["#76714E", "#E8C7A6", "#E0A658", "#C24F46", "#456A6C"],
        id: "1000-14"
      }
    ],
    posters: [{
        bgCol: "#1A2947",
        image: "/assets/posters/1000/0.jpg",
        colors: ["#F4E8B8", "#EEC30E", "#8F0100", "#058187", "#0C4DA7"]
      },
      {
        bgCol: "#95876E",
        image: "/assets/posters/1000/1.jpg",
        colors: ["#B9B2A3", "#404948", "#D6AF6C", "#B44541", "#632621"]
      },
      {
        bgCol: "#E5D9C3",
        image: "/assets/posters/1000/2.jpg",
        colors: ["#CDA561", "#AB672D", "#CAC2B3", "#989FB1", "#174073"]
      }
    ]
  },
  "1700": {
    palettes: [{
        name: "Officer",
        colors: ["#D5C2A2", "#CB9323", "#926D33", "#3F5756", "#172639"],
        id: "1700-0"
      },
      {
        name: "Friendship",
        colors: ["#F7A38A", "#F48A5B", "#B7A78D", "#DEAE62", "#524A47"],
        id: "1700-1"
      },
      {
        name: "The Dress",
        colors: ["#5A8693", "#DFB064", "#E8E5DE", "#B3BEB0", "#576359"],
        id: "1700-2"
      },
      {
        name: "Long Rest",
        colors: ["#6C8194", "#B8985B", "#86373A", "#293447", "#443240"],
        id: "1700-3"
      },
      {
        name: "Proper Lady",
        colors: ["#ECE8DF", "#FABC55", "#D08642", "#A04533", "#59231A"],
        id: "1700-4"
      },
      {
        name: "Decor",
        colors: ["#CD9D4F", "#9D7538", "#277D53", "#275D3D", "#222B1C"],
        id: "1700-5"
      },
      {
        name: "Warrior",
        colors: ["#EEB765", "#748D88", "#E7B489", "#D59576", "#564E4B"],
        id: "1700-6"
      },
      {
        name: "Family Time",
        colors: ["#6AA1B6", "#799D83", "#E2CE91", "#AD917C", "#D66E61"],
        id: "1700-7"
      },
      {
        name: "First Love",
        colors: ["#BFA87E", "#B1906D", "#626A5D", "#4F5144", "#342818"],
        id: "1700-8"
      },
      {
        name: "Raging Sea",
        colors: ["#E0CCB1", "#C1A185", "#B0836B", "#AEB2A1", "#7E8072"],
        id: "1700-9"
      },
      {
        name: "Gentleman",
        colors: ["#EDE5D5", "#AC9A82", "#6A5A43", "#347C8D", "#1A444C"],
        id: "1700-10"
      },
      {
        name: "Safe Place",
        colors: ["#E2DED2", "#ACA794", "#B3CFDB", "#8FAFBC", "#5B737D"],
        id: "1700-11"
      },
      {
        name: "Have A Seat",
        colors: ["#EECF95", "#CAA067", "#B78742", "#614B28", "#3F240B"],
        id: "1700-12"
      },
      {
        name: "Leg Work",
        colors: ["#E2BF4A", "#913527", "#BCB297", "#767051", "#4F483D"],
        id: "1700-13"
      },
      {
        name: "Remembrance",
        colors: ["#E2BA9A", "#D0A07B", "#E5A367", "#9D652E", "#212530"],
        id: "1700-14"
      }
    ],
    posters: [{
        bgCol: "#7B6E58",
        image: "/assets/posters/1700/0.jpg",
        colors: ["#D5C2A2", "#CB9323", "#926D33", "#3F5756", "#172639"]
      },
      {
        bgCol: "#7A6B65",
        image: "/assets/posters/1700/1.jpg",
        colors: ["#F7A38A", "#F48A5B", "#B7A78D", "#DEAE62", "#524A47"]
      },
      {
        bgCol: "#6F8072",
        image: "/assets/posters/1700/2.jpg",
        colors: ["#5A8693", "#DFB064", "#E8E5DE", "#B3BEB0", "#576359"]
      }
    ]
  },
  "1800": {
    palettes: [{
        name: "Venice",
        colors: ["#878E7C", "#7E7C43", "#EEA23B", "#D8491D", "#855C3E"],
        id: "1800-0"
      },
      {
        name: "Camp Meeting",
        colors: ["#ECD0BB", "#D26E3A", "#929BAC", "#013D6F", "#071E44"],
        id: "1800-1"
      },
      {
        name: "Red Letter",
        colors: ["#CA382B", "#E4D4B3", "#B3A692", "#938775", "#373634"],
        id: "1800-2"
      },
      {
        name: "Horse and Buggy",
        colors: ["#EEE1CD", "#E1A850", "#79715C", "#53676E", "#2A2E31"],
        id: "1800-3"
      },
      {
        name: "Brightest Star",
        colors: ["#FCECC3", "#F1DB8E", "#8D9D81", "#63806C", "#3D5958"],
        id: "1800-4"
      },
      {
        name: "Open Window",
        colors: ["#F0DFC3", "#DEA93D", "#A6785E", "#7F6765", "#50464E"],
        id: "1800-5"
      },
      {
        name: "Rose Bush",
        colors: ["#F3E2C8", "#F0C789", "#A6B098", "#4B795C", "#BC2744"],
        id: "1800-6"
      },
      {
        name: "Mustache",
        colors: ["#F36B3C", "#EBD7BC", "#C9B099", "#84674C", "#3E3832"],
        id: "1800-7"
      },
      {
        name: "Coach",
        colors: ["#D7BF9B", "#DAB04E", "#BE6E33", "#68603C", "#585451"],
        id: "1800-8"
      },
      {
        name: "Sandy Shores",
        colors: ["#E8E1C9", "#EBD17C", "#F0623C", "#566680", "#28282A"],
        id: "1800-9"
      },
      {
        name: "Bloom",
        colors: ["#F0C95A", "#DB8647", "#CA443B", "#62968E", "#5E5C67"],
        id: "1800-10"
      },
      {
        name: "Sunny Day",
        colors: ["#EAD2BA", "#F3B234", "#F56D4E", "#2B3E4D", "#332A2A"],
        id: "1800-11"
      },
      {
        name: "Through the Park",
        colors: ["#F1DDC5", "#E3B79E", "#DF5842", "#877E68", "#322E2D"],
        id: "1800-12"
      },
      {
        name: "Daily News",
        colors: ["#EDD9C0", "#F3A548", "#DA393F", "#186C8F", "#42353E"],
        id: "1800-13"
      },
      {
        name: "Meadow",
        colors: ["#8C8D41", "#6E6E42", "#5A5F45", "#766339", "#303325"],
        id: "1800-14"
      }
    ],
    posters: [{
        bgCol: "#F2D9C7",
        image: "/assets/posters/1800/0.jpg",
        colors: ["#878E7C", "#7E7C43", "#EEA23B", "#D8491D", "#855C3E"]
      },
      {
        bgCol: "#5C88AD",
        image: "/assets/posters/1800/1.jpg",
        colors: ["#ECD0BB", "#D26E3A", "#929BAC", "#013D6F", "#071E44"]
      },
      {
        bgCol: "#6E6962",
        image: "/assets/posters/1800/2.jpg",
        colors: ["#CA382B", "#E4D4B3", "#B3A692", "#938775", "#373634"]
      }
    ]
  },
  "1900": {
    palettes: [{
        name: "Hooligan",
        colors: ["#EAB57F", "#4A7D90", "#939249", "#E3AE3C", "#C24F1A"],
        id: "1900-0"
      },
      {
        name: "Blue Moon",
        colors: ["#D6C3B5", "#9E997B", "#5B5E4A", "#3B3D30", "#192320"],
        id: "1900-1"
      },
      {
        name: "Showman",
        colors: ["#CEBFB9", "#71614C", "#B49E6F", "#C17121", "#3C371A"],
        id: "1900-2"
      },
      {
        name: "Man of the Hour",
        colors: ["#E6AA30", "#D9892B", "#C33809", "#766135", "#46381D"],
        id: "1900-3"
      },
      {
        name: "Dance",
        colors: ["#C48D54", "#9C6C58", "#62685C", "#22404A", "#243431"],
        id: "1900-4"
      },
      {
        name: "Journal",
        colors: ["#E0DCD9", "#AA9965", "#7E7A5A", "#414538", "#1E2428"],
        id: "1900-5"
      },
      {
        name: "Case Closed",
        colors: ["#EBC371", "#DEAA44", "#AC7534", "#706738", "#332C1A"],
        id: "1900-6"
      },
      {
        name: "Afternoon Stroll",
        colors: ["#88B2CA", "#204D72", "#FA971E", "#979089", "#3A3C39"],
        id: "1900-7"
      },
      {
        name: "Burden",
        colors: ["#7C7848", "#E3C736", "#6A7490", "#9D1F35", "#5D242A"],
        id: "1900-8"
      },
      {
        name: "Autumn",
        colors: ["#E7D1A9", "#F1C202", "#807474", "#C5484C", "#633C3E"],
        id: "1900-9"
      },
      {
        name: "The Pauper",
        colors: ["#A99142", "#86473E", "#737B6C", "#4D6970", "#5F4E4D"],
        id: "1900-10"
      },
      {
        name: "Youth",
        colors: ["#5CA0A3", "#E3B13F", "#E86B4B", "#D33F4A", "#8E5133"],
        id: "1900-11"
      },
      {
        name: "Game Time",
        colors: ["#21809E", "#D8C19F", "#B98748", "#AD0327", "#2D1A14"],
        id: "1900-12"
      },
      {
        name: "Rustic",
        colors: ["#C8A87F", "#D49E34", "#C29042", "#614B3C", "#503E3A"],
        id: "1900-13"
      },
      {
        name: "Well Water",
        colors: ["#E7C484", "#7D9461", "#687E6D", "#C54A25", "#8C5625"],
        id: "1900-14"
      }
    ],
    posters: [{
        bgCol: "#FDE5CB",
        image: "/assets/posters/1900/0.jpg",
        colors: ["#EAB57F", "#4A7D90", "#939249", "#E3AE3C", "#C24F1A"]
      },
      {
        bgCol: "#7D7F71",
        image: "/assets/posters/1900/1.jpg",
        colors: ["#D6C3B5", "#9E997B", "#5B5E4A", "#3B3D30", "#192320"]
      },
      {
        bgCol: "#999161",
        image: "/assets/posters/1900/2.jpg",
        colors: ["#CEBFB9", "#71614C", "#B49E6F", "#C17121", "#3C371A"]
      }
    ]
  },
  "1910": {
    palettes: [{
        name: "Girl from Paris",
        colors: ["#EBD7C3", "#E7B67B", "#D17028", "#809181", "#313C3E"],
        id: "1910-0"
      },
      {
        name: "Shining City",
        colors: ["#DCC978", "#D39E04", "#82875D", "#4B5733", "#1E2214"],
        id: "1910-1"
      },
      {
        name: "Home and Country",
        colors: ["#4A564C", "#C19E5C", "#7C531A", "#7C3314", "#572D14"],
        id: "1910-2"
      },
      {
        name: "Race Day",
        colors: ["#F5F8F2", "#919484", "#FAB072", "#F57624", "#A33318"],
        id: "1910-3"
      },
      {
        name: "Geometry",
        colors: ["#778B88", "#F4DDA4", "#E89104", "#C12205", "#7C4C10"],
        id: "1910-4"
      },
      {
        name: "Story Time",
        colors: ["#94A08A", "#F6D766", "#D78A72", "#7D8EAD", "#7E6A7B"],
        id: "1910-5"
      },
      {
        name: "Temptation",
        colors: ["#E5D0A3", "#DB9B75", "#DB1B12", "#2F546D", "#0B2540"],
        id: "1910-6"
      },
      {
        name: "Rolling Hills",
        colors: ["#DEB555", "#E28F0C", "#477F9A", "#86A201", "#295934"],
        id: "1910-7"
      },
      {
        name: "The Dragon",
        colors: ["#EAA651", "#E76134", "#E73430", "#506536", "#1A2129"],
        id: "1910-8"
      },
      {
        name: "Plowing",
        colors: ["#BED180", "#63A3BC", "#D3A27E", "#FD8E59", "#4D4341"],
        id: "1910-9"
      },
      {
        name: "Runaway",
        colors: ["#959365", "#C2B2A2", "#A3947F", "#A6522D", "#7A4938"],
        id: "1910-10"
      },
      {
        name: "Town Hall",
        colors: ["#F7EEE2", "#B0C8CA", "#FB8C82", "#BFA16F", "#312613"],
        id: "1910-11"
      },
      {
        name: "Wave Goodbye",
        colors: ["#E3D3BA", "#E5A40F", "#D3742B", "#869D8C", "#445A54"],
        id: "1910-12"
      },
      {
        name: "Tomboy",
        colors: ["#65B1AF", "#D44634", "#E4C8A9", "#A889A3", "#6F4B5E"],
        id: "1910-13"
      },
      {
        name: "Let It Shine",
        colors: ["#D9C06B", "#D7714D", "#52675D", "#245168", "#4E3F43"],
        id: "1910-14"
      }
    ],
    posters: [{
        bgCol: "#9C7354",
        image: "/assets/posters/1910/0.jpg",
        colors: ["#EBD7C3", "#E7B67B", "#D17028", "#809181", "#313C3E"]
      },
      {
        bgCol: "#3B412E",
        image: "/assets/posters/1910/1.jpg",
        colors: ["#DCC978", "#D39E04", "#82875D", "#4B5733", "#1E2214"]
      },
      {
        bgCol: "#978562",
        image: "/assets/posters/1910/2.jpg",
        colors: ["#4A564C", "#C19E5C", "#7C531A", "#7C3314", "#572D14"]
      }
    ]
  },
  "1920": {
    palettes: [{
        name: "Magic Man",
        colors: ["#E5A914", "#DC0502", "#005E2A", "#058F96", "#031F34"],
        id: "1920-0"
      },
      {
        name: "Back to Nature",
        colors: ["#A59E69", "#C99946", "#C05934", "#57897B", "#34362F"],
        id: "1920-1"
      },
      {
        name: "Venezia",
        colors: ["#E8B797", "#EDB126", "#AA341B", "#302A46", "#2D1A1E"],
        id: "1920-2"
      },
      {
        name: "Warmth",
        colors: ["#FBC500", "#D4340A", "#90241C", "#3D1C66", "#1C1140"],
        id: "1920-3"
      },
      {
        name: "Eating Out",
        colors: ["#CEE5CB", "#F9C61F", "#B9111E", "#015F9F", "#08070F"],
        id: "1920-4"
      },
      {
        name: "The Future",
        colors: ["#F3E2C8", "#FEF200", "#F7941D", "#ED1B24", "#0270BA"],
        id: "1920-5"
      },
      {
        name: "Express",
        colors: ["#FBC592", "#A07739", "#513207", "#EB3B24", "#324885"],
        id: "1920-6"
      },
      {
        name: "Library",
        colors: ["#AFA856", "#DA8851", "#878E87", "#AB3855", "#292A2D"],
        id: "1920-7"
      },
      {
        name: "Smooth Sailing",
        colors: ["#F2D7BD", "#496257", "#F9B93F", "#356281", "#2C3233"],
        id: "1920-8"
      },
      {
        name: "In Thought",
        colors: ["#D8B888", "#CE8F59", "#99697E", "#79445E", "#421C31"],
        id: "1920-9"
      },
      {
        name: "Success",
        colors: ["#F4CFA5", "#FDAA26", "#FF691B", "#636B35", "#3E2A1F"],
        id: "1920-10"
      },
      {
        name: "Docked",
        colors: ["#D6B980", "#D2A639", "#8D3004", "#3E451B", "#1B191A"],
        id: "1920-11"
      },
      {
        name: "Iron Horse",
        colors: ["#DFE5DD", "#A8989A", "#C73401", "#5C3D26", "#170F02"],
        id: "1920-12"
      },
      {
        name: "Forever Changing",
        colors: ["#7BA99E", "#E0AA24", "#A05300", "#481929", "#192637"],
        id: "1920-13"
      },
      {
        name: "Drink This",
        colors: ["#8AB889", "#E3B38F", "#F0EAC7", "#E6A93E", "#4F4D34"],
        id: "1920-14"
      }
    ],
    posters: [{
        bgCol: "#85D7D2",
        image: "/assets/posters/1920/0.jpg",
        colors: ["#E5A914", "#DC0502", "#005E2A", "#058F96", "#031F34"]
      },
      {
        bgCol: "#686E59",
        image: "/assets/posters/1920/1.jpg",
        colors: ["#A59E69", "#C99946", "#C05934", "#57897B", "#34362F"]
      },
      {
        bgCol: "#5D586F",
        image: "/assets/posters/1920/2.jpg",
        colors: ["#E8B797", "#EDB126", "#AA341B", "#302A46", "#2D1A1E"]
      }
    ]
  },
  "1930": {
    palettes: [{
        name: "Blind Alley",
        colors: ["#F9E3B8", "#EBB85D", "#DB9501", "#D26500", "#25251B"],
        id: "1930-0"
      },
      {
        name: "Don’t Mix Em",
        colors: ["#C14F11", "#773621", "#ABB1A5", "#1D354B", "#0E0D1E"],
        id: "1930-1"
      },
      {
        name: "Traveler",
        colors: ["#C1CDDC", "#516EBC", "#00529C", "#153477", "#050F2B"],
        id: "1930-2"
      },
      {
        name: "Top Hat",
        colors: ["#DAD9C4", "#EBB821", "#D98025", "#BF4430", "#33302A"],
        id: "1930-3"
      },
      {
        name: "Sea Cliff",
        colors: ["#EAE5C6", "#F4DA61", "#DB8479", "#6179B5", "#272D2D"],
        id: "1930-4"
      },
      {
        name: "Tradition",
        colors: ["#F4D6C3", "#EFB16C", "#828461", "#AB4253", "#485F66"],
        id: "1930-5"
      },
      {
        name: "Watch Tower",
        colors: ["#F7DF19", "#B63E30", "#D9D9CD", "#85847F", "#1A1C27"],
        id: "1930-6"
      },
      {
        name: "Symphony",
        colors: ["#E3BE76", "#CE9F29", "#B85B2B", "#506058", "#674230"],
        id: "1930-7"
      },
      {
        name: "Help Yourself",
        colors: ["#E6D8B6", "#D6B683", "#DEA500", "#8FA364", "#514A38"],
        id: "1930-8"
      },
      {
        name: "Canyon",
        colors: ["#A1A799", "#B06034", "#C29288", "#7E737E", "#273443"],
        id: "1930-9"
      },
      {
        name: "Castle Wall",
        colors: ["#8AA5A0", "#50566E", "#C3B47B", "#7E8A63", "#404A3B"],
        id: "1930-10"
      },
      {
        name: "Eruption",
        colors: ["#D9CDB3", "#B3C9BD", "#497791", "#666E7F", "#333947"],
        id: "1930-11"
      },
      {
        name: "Costume",
        colors: ["#CECABE", "#D8B321", "#B33447", "#505D78", "#3A465F"],
        id: "1930-12"
      },
      {
        name: "Honest Abe",
        colors: ["#D4C397", "#8A794B", "#B7BF85", "#929E57", "#3C473C"],
        id: "1930-13"
      },
      {
        name: "Tribes",
        colors: ["#38526E", "#DAC9AF", "#C19C82", "#AE7757", "#6C4838"],
        id: "1930-14"
      }
    ],
    posters: [{
        bgCol: "#917D53",
        image: "/assets/posters/1930/0.jpg",
        colors: ["#F9E3B8", "#EBB85D", "#DB9501", "#D26500", "#25251B"]
      },
      {
        bgCol: "#465B77",
        image: "/assets/posters/1930/1.jpg",
        colors: ["#C14F11", "#773621", "#ABB1A5", "#1D354B", "#0E0D1E"]
      },
      {
        bgCol: "#2A89E0",
        image: "/assets/posters/1930/2.jpg",
        colors: ["#C1CDDC", "#516EBC", "#00529C", "#153477", "#050F2B"]
      }
    ]
  },
  "1940": {
    palettes: [{
        name: "We Can Do It",
        colors: ["#F6C970", "#FAE804", "#EE1D19", "#0E68AD", "#031E4B"],
        id: "1940-0"
      },
      {
        name: "Exhibition",
        colors: ["#909CA5", "#F1CE54", "#CF540D", "#6EA2C3", "#181E34"],
        id: "1940-1"
      },
      {
        name: "Handle With Care",
        colors: ["#DF9D09", "#C22E14", "#446B4C", "#D9B787", "#493F24"],
        id: "1940-2"
      },
      {
        name: "Armed",
        colors: ["#FFFFFF", "#FEC3F0", "#FA110D", "#54282A", "#2D2D2D"],
        id: "1940-3"
      },
      {
        name: "Heads Up",
        colors: ["#CCCECD", "#8A8278", "#3B678C", "#D4A78D", "#CD4E48"],
        id: "1940-4"
      },
      {
        name: "Parade",
        colors: ["#E3D697", "#E4AD2E", "#AA4534", "#606574", "#3F4064"],
        id: "1940-5"
      },
      {
        name: "Swirl",
        colors: ["#F4F4F2", "#AFC8B8", "#4881B2", "#DE443A", "#9E3E32"],
        id: "1940-6"
      },
      {
        name: "Comfortable",
        colors: ["#D2998D", "#D24541", "#EDDECB", "#BFB8A6", "#615B5B"],
        id: "1940-7"
      },
      {
        name: "Commencement",
        colors: ["#FDF6EE", "#EDDABA", "#B2B6A0", "#E66111", "#000000"],
        id: "1940-8"
      },
      {
        name: "Projections",
        colors: ["#FDA176", "#FD4A1F", "#FEE9B0", "#F4B04D", "#58B185"],
        id: "1940-9"
      },
      {
        name: "Farm House",
        colors: ["#F4F2ED", "#DAD5CF", "#6B83AB", "#CF4626", "#45423E"],
        id: "1940-10"
      },
      {
        name: "Standing Tall",
        colors: ["#FDE58E", "#F2CA66", "#73D6F5", "#077F92", "#1C2227"],
        id: "1940-11"
      },
      {
        name: "Wasteland",
        colors: ["#DDC289", "#E1AB00", "#C95635", "#4B4D54", "#39362D"],
        id: "1940-12"
      },
      {
        name: "Language",
        colors: ["#E9E3D3", "#F1DA00", "#E10602", "#33453D", "#2E3035"],
        id: "1940-13"
      },
      {
        name: "Architecture",
        colors: ["#982312", "#6A140C", "#E1DFAE", "#9CA888", "#272020"],
        id: "1940-14"
      }
    ],
    posters: [{
        bgCol: "#456AA7",
        image: "/assets/posters/1940/0.jpg",
        colors: ["#F6C970", "#FAE804", "#EE1D19", "#0E68AD", "#031E4B"]
      },
      {
        bgCol: "#607080",
        image: "/assets/posters/1940/1.jpg",
        colors: ["#909CA5", "#F1CE54", "#CF540D", "#6EA2C3", "#181E34"]
      },
      {
        bgCol: "#787050",
        image: "/assets/posters/1940/2.jpg",
        colors: ["#DF9D09", "#C22E14", "#446B4C", "#D9B787", "#493F24"]
      }
    ]
  },
  "1950": {
    palettes: [{
        name: "New York",
        colors: ["#FFDA32", "#EA2768", "#4174B3", "#5A4D88", "#534856"],
        id: "1950-0"
      },
      {
        name: "Outer Space",
        colors: ["#3565A3", "#023D79", "#F5D5D6", "#FA2D19", "#6A1012"],
        id: "1950-1"
      },
      {
        name: "Skull",
        colors: ["#FEDEB8", "#ABDACB", "#FDFD00", "#FD4126", "#2C4743"],
        id: "1950-2"
      },
      {
        name: "Chemistry",
        colors: ["#94AFC0", "#2289B6", "#F7BD40", "#E0160A", "#871008"],
        id: "1950-3"
      },
      {
        name: "Fizz",
        colors: ["#9DAFA5", "#F4E1B5", "#E0C38A", "#DA391B", "#2D2F23"],
        id: "1950-4"
      },
      {
        name: "Product Placement",
        colors: ["#4E84BA", "#F9EBC9", "#F0D137", "#E95913", "#5F5B49"],
        id: "1950-5"
      },
      {
        name: "Vintage",
        colors: ["#FBBB04", "#D53A12", "#6B7C84", "#686444", "#421F04"],
        id: "1950-6"
      },
      {
        name: "Framed",
        colors: ["#FEF2AB", "#E5D098", "#4F7379", "#385A5C", "#000506"],
        id: "1950-7"
      },
      {
        name: "Buckle Up",
        colors: ["#EECB28", "#A05A48", "#E4E2E3", "#807C8A", "#14121E"],
        id: "1950-8"
      },
      {
        name: "Jazz Night",
        colors: ["#EDE3CB", "#E6D53C", "#81ABAA", "#EF594B", "#443A38"],
        id: "1950-9"
      },
      {
        name: "Business As Usual",
        colors: ["#F8E4BC", "#F4D825", "#DF4C0F", "#6F86B6", "#0E151D"],
        id: "1950-10"
      },
      {
        name: "Thriller",
        colors: ["#E6EDF6", "#9498A1", "#443A45", "#FE3B2C", "#74221F"],
        id: "1950-11"
      },
      {
        name: "Bus Stop",
        colors: ["#F9CF94", "#FEAF4B", "#DE3211", "#2D337B", "#0A0904"],
        id: "1950-12"
      },
      {
        name: "Stardom",
        colors: ["#EFD5A1", "#F6C618", "#CD5183", "#5F91A6", "#1F5182"],
        id: "1950-13"
      },
      {
        name: "Red Hot",
        colors: ["#F8F1E2", "#E4E0C2", "#E7C600", "#DD1400", "#120D0A"],
        id: "1950-14"
      }
    ],
    posters: [{
        bgCol: "#7566a8",
        image: "/assets/posters/1950/0.jpg",
        colors: ["#FFDA32", "#EA2768", "#4174B3", "#5A4D88", "#534856"]
      },
      {
        bgCol: "#3F556C",
        image: "/assets/posters/1950/1.jpg",
        colors: ["#3565A3", "#023D79", "#F5D5D6", "#FA2D19", "#6A1012"]
      },
      {
        bgCol: "#443B39",
        image: "/assets/posters/1950/2.jpg",
        colors: ["#FEDEB8", "#ABDACB", "#FDFD00", "#FD4126", "#2C4743"]
      }
    ]
  },
  "1960": {
    palettes: [{
        name: "The Brain",
        colors: ["#DF1C0A", "#FFFEFA", "#D79F90", "#77524C", "#262425"],
        id: "1960-0"
      },
      {
        name: "X-Ray",
        colors: ["#FDE001", "#A2C551", "#696FA4", "#5E2E28", "#41293A"],
        id: "1960-1"
      },
      {
        name: "Reptile",
        colors: ["#C24328", "#FCBC29", "#A2AB28", "#36907E", "#3B4E3F"],
        id: "1960-2"
      },
      {
        name: "Orbit",
        colors: ["#96D5A2", "#80B588", "#FFA64C", "#F5410A", "#36251B"],
        id: "1960-3"
      },
      {
        name: "Wildlife",
        colors: ["#FFF9D6", "#FFCC08", "#FF2212", "#103391", "#29241E"],
        id: "1960-4"
      },
      {
        name: "Double Decker",
        colors: ["#7E95BF", "#FAA26C", "#964A34", "#DF4120", "#2C150D"],
        id: "1960-5"
      },
      {
        name: "Progress",
        colors: ["#497896", "#4E6337", "#EE6E39", "#CE473E", "#562D2B"],
        id: "1960-6"
      },
      {
        name: "Together",
        colors: ["#EAD8CA", "#CEABB0", "#4D6181", "#C6B74F", "#F46F21"],
        id: "1960-7"
      },
      {
        name: "A Trim",
        colors: ["#B75C22", "#468852", "#E1F3F6", "#749190", "#1D3929"],
        id: "1960-8"
      },
      {
        name: "Tweet",
        colors: ["#CCA37A", "#C57E02", "#C44207", "#726851", "#3F2412"],
        id: "1960-9"
      },
      {
        name: "Short Story",
        colors: ["#F15A00", "#B43200", "#B6B2A9", "#555752", "#1B1F22"],
        id: "1960-10"
      },
      {
        name: "Whimsy",
        colors: ["#C87994", "#C3D2CF", "#52A29F", "#283730", "#0E0312"],
        id: "1960-11"
      },
      {
        name: "Don’t Milk It",
        colors: ["#FDA490", "#FF8885", "#811648", "#5E113D", "#051936"],
        id: "1960-12"
      },
      {
        name: "Another Town",
        colors: ["#EBC56A", "#C93825", "#A1AEC6", "#738EA4", "#5E5981"],
        id: "1960-13"
      },
      {
        name: "After Hours",
        colors: ["#F05899", "#F1DF02", "#EAA503", "#E44900", "#020158"],
        id: "1960-14"
      }
    ],
    posters: [{
        bgCol: "#AB8379",
        image: "/assets/posters/1960/0.jpg",
        colors: ["#DF1C0A", "#FFFEFA", "#D79F90", "#77524C", "#262425"]
      },
      {
        bgCol: "#565C6C",
        image: "/assets/posters/1960/1.jpg",
        colors: ["#FDE001", "#A2C551", "#696FA4", "#5E2E28", "#41293A"]
      },
      {
        bgCol: "#FFE0A0",
        image: "/assets/posters/1960/2.jpg",
        colors: ["#C24328", "#FCBC29", "#A2AB28", "#36907E", "#3B4E3F"]
      }
    ]
  }
};

CDDATES = [
  "2000BC",
  "0",
  "1000",
  "1700",
  "1800",
  "1900",
  "1910",
  "1920",
  "1930",
  "1940",
  "1950",
  "1960"
];

CDBADGES = [{
    date: "2000BC",
    src: "/assets/badges/2000BC.png"
  },
  {
    date: "0",
    src: "/assets/badges/0.png"
  },
  {
    date: "1000",
    src: "/assets/badges/1000.png"
  },
  {
    date: "1700",
    src: "/assets/badges/1700.png"
  },
  {
    date: "1800",
    src: "/assets/badges/1800.png"
  },
  {
    date: "1900",
    src: "/assets/badges/1900.png"
  },
  {
    date: "1910",
    src: "/assets/badges/1910.png"
  },
  {
    date: "1920",
    src: "/assets/badges/1920.png"
  },
  {
    date: "1930",
    src: "/assets/badges/1930.png"
  },
  {
    date: "1940",
    src: "/assets/badges/1940.png"
  },
  {
    date: "1950",
    src: "/assets/badges/1950.png"
  },
  {
    date: "1960",
    src: "/assets/badges/1960.png"
  }
];

CDTEXT = {
  "2000BC": "Heavily stylized and symbolic art was often used in structures such as tombs and on monuments. Paintings in these areas had a heavy focus on the afterlife.",
  "0": "Art played a large part in the everyday lives of people in ancient societies. Marble statues, painted pottery, and large murals were cultural cornerstones.",
  "1000": "Religious figures and stories were the subject matter of a vast amount of art in both print and architecture during this age which was home to the Renaissance.",
  "1700": "Colors of the Renaissance were met with new modern palettes. Styles varied across the world from realistic brush paintings to simplified illustrations.",
  "1800": "Chromolithography enabled printing of this era’s designs, which were highly ornate, filled with organic lines, and often contained a large number of elements.",
  "1900": "The modern advertising industry was born. Intricate illustrations and hand-drawn typography were used alongside natural forms and various textures.",
  "1910": "Color printing became widespread, which drove the creation of character-focused, colorful painted figures and scenes, as opposed to ink drawings.",
  "1920": "Intricate, hand-drawn styles gave way to the birth of an international modernist movement. Designs were filled with rich colors and geometric, minimalist shapes.",
  "1930": "In light of the Great Depression, colors took on more subtle muted tones. Forms shifted toward a futuristic, technologically-driven direction, within clean lines.",
  "1940": "The beginning of World War 2 brought about an era that produced propaganda and patriotic imagery that relied on minimal color palettes and strong slogans.",
  "1950": "Advertisements were filled with excitement and movement, often with a focus on beautiful women or family life. Creative typography also came onto the scene.",
  "1960": "Swiss design, known for its use of layout grids and negative space, became prominent. This was contrasted by the new use of psychedelic illustrations and colors."
}
