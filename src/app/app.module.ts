import { NgModule, isDevMode } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./routing.module";
import { AppComponent } from "./app.component";
import { Store } from "./store";
import { HttpClientModule } from "@angular/common/http";
import { ServiceWorkerModule } from "@angular/service-worker";
import { AuthModule } from "./shared/auth/auth.module";

import { provideFirebaseApp, initializeApp } from "@angular/fire/app";
import { getFirestore, provideFirestore } from "@angular/fire/firestore";
import { getAuth, provideAuth } from "@angular/fire/auth";

@NgModule({
  declarations: [AppComponent],
  imports: [
    provideFirebaseApp(() => initializeApp({
  apiKey: "AIzaSyBmBGr-pjg1UxJtOD95HLmXPKoKPbh5W2s",
  authDomain: "color-leap-59240.firebaseapp.com",
  databaseURL: "https://color-leap-59240.firebaseio.com",
  projectId: "color-leap-59240",
  storageBucket: "color-leap-59240.appspot.com",
  messagingSenderId: "192427945596"
})),
    provideFirestore(() => getFirestore()),
    provideAuth(() => getAuth()),
    AuthModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: "registerWhenStable:30000",
    }),
    
  ],
  providers: [Store],
  bootstrap: [AppComponent],
})
export class AppModule {}
