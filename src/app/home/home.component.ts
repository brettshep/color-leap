import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Badge } from "../../../interfaces";
import { BehaviorSubject, interval, Subscription } from "rxjs";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.sass"]
})
export class HomeComponent implements OnInit {
  badges: Badge[];
  bgColors: { bg: string; fg: string }[] = [
    { bg: "#EFDEC4", fg: "#EE8927" },
    { bg: "#497B9E ", fg: "#F9B93F" },
    { bg: "#99697E", fg: "#D8B888" },
    { bg: "#F2CA66", fg: "#077F92" },
    { bg: "#63666F", fg: "#DDC289" },
    { bg: "#8C8D41", fg: "#565B41" },
    { bg: "#C8475A", fg: "#F9B93F" },
    { bg: "#809181", fg: "#DBC4AC" },
    { bg: "#486665", fg: "#CB9323" },
    { bg: "#666E7F", fg: "#B3C9BD" }
  ];
  bgIndex = 0;
  bgColor$: BehaviorSubject<{ bg: string; fg: string }> = new BehaviorSubject({
    ...this.bgColors[0]
  });
  sub: Subscription;

  ngOnInit() {
    this.badges = [...CDBADGES, ...CDBADGES.slice(0, 8)];
    this.sub = interval(3000).subscribe(() => {
      this.bgIndex++;
      if (this.bgIndex >= this.bgColors.length) this.bgIndex = 0;
      this.bgColor$.next({ ...this.bgColors[this.bgIndex] });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
