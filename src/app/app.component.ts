import { RouteAnimations } from "./routeAnimations";
import { map } from "rxjs/operators";
import { Store } from "./store";
import { Observable, Subscription } from "rxjs";
import { Component, ChangeDetectorRef, ViewChild } from "@angular/core";

import { RouterOutlet } from "@angular/router";
import { User } from "../../interfaces";
import { AuthService } from "./shared/auth/auth.service";
import { CopyService } from "./shared/copy.service";

@Component({
  selector: "app-root",
  template: `
    <div class="page" [@routeAnimation]="getDepth(myOutlet)">
      <nav class="navBar">
        <a routerLink="/dates">
          <div class="dates">
            <i class="far fa-calendar-alt"></i>
          </div>
        </a>
        <div class="socialCont">
          <h3>SHARE ON</h3>
          <div class="socialBtnCont">
            <a
              target="_blank"
              href="https://www.facebook.com/sharer/sharer.php?u=https%3A//colorleap.app"
            >
              <div id="facebook" class="share">
                <i class="fab fa-facebook-f"></i>
              </div>
            </a>
            <a
              target="_blank"
              href="https://twitter.com/home?status=https%3A//colorleap.app"
            >
              <div id="twitter" class="share">
                <i class="fab fa-twitter"></i>
              </div>
            </a>
          </div>
        </div>
        <ng-container *ngIf="user$ | async as user; else signIn">
          <a routerLink="/likes">
            <div id="likes" class="account">
              {{ totalLikes }}
              <i class="fas fa-heart"></i>
            </div>
          </a>
        </ng-container>
        <ng-template #signIn>
          <div id="signIn" class="account" (click)="openAuth()">SIGN IN</div>
        </ng-template>
      </nav>
      <router-outlet #myOutlet="outlet"></router-outlet>

      <!--AUTH MODAL-->
      <ng-container *ngIf="authOpen">
        <auth
          @FadeInOut
          [errorStatus]="errorStatus"
          (signIn)="authSignIn($event)"
          (exit)="closeAuth()"
        >
        </auth>
      </ng-container>
    </div>

    <div *ngIf="spinner" class="spinner"></div>

    <div class="copy" #copy>COPIED!</div>
  `,
  styleUrls: ["./app.component.sass"],
  animations: RouteAnimations,
})
export class AppComponent {
  @ViewChild("copy")
  copyElem;
  user$: Observable<User>;
  authOpen: boolean = false;
  subscriptions: Subscription[] = [];
  errorStatus: "error" | "success" = null;
  totalLikes: number;
  spinner: boolean = false;
  constructor(
    private store: Store,
    private authServ: AuthService,
    private cd: ChangeDetectorRef,
    private copyServ: CopyService
  ) {}

  //------LIFE CYCLE---------
  ngOnInit() {
    //user
    this.subscriptions.push(this.authServ.user$.subscribe());
    this.user$ = this.store.select<User>("user");

    //auth modal
    this.subscriptions.push(
      this.authServ.openModal$.subscribe(() => {
        this.authOpen = true;
        this.errorStatus = null;
      })
    );

    //total likes
    this.subscriptions.push(
      this.store
        .select<User>("user")
        .pipe(
          map((val) => {
            if (val) {
              return val.likes.length;
            } else {
              return undefined;
            }
          })
        )
        .subscribe((val) => {
          if (val !== undefined) {
            this.totalLikes = val;
          }
        })
    );

    //spinner wheel
    this.subscriptions.push(
      this.authServ.showSpinner$.subscribe((val) => {
        if (val) {
          this.spinner = true;
        } else {
          this.spinner = false;
        }
      })
    );

    //copy
    this.subscriptions.push(
      this.copyServ.copied$.subscribe(() => {
        //show copy popup
        const copy: HTMLDivElement = this.copyElem.nativeElement;
        copy.classList.remove("active");
        void copy.offsetWidth;
        setTimeout(() => {
          copy.classList.add("active");
        }, 0);
      })
    );

    this.authServ.checkRedirect();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
    });
  }

  //-----AUTH--------

  openAuth() {
    this.authServ.openModal$.emit();
  }

  closeAuth() {
    setTimeout(() => {
      this.authOpen = false;
      this.cd.detectChanges();
    }, 0);
  }

  authSignIn(e: {
    provider: "google" | "twitter" | "facebook";
    popUp: boolean;
  }) {
    if (e.popUp) {
      this.authSignInPopUp(e.provider);
    } else {
      this.authSignInRedirect(e.provider);
    }
  }

  async authSignInPopUp(provider: "google" | "twitter" | "facebook") {
    if (provider === "google") {
      try {
        await this.authServ.googleLogin(true);
        this.errorStatus = "success";
      } catch (err) {
        this.errorStatus = "error";
      }
    } else {
      try {
        await this.authServ.facebookLogin(true);
        this.errorStatus = "success";
      } catch (err) {
        this.errorStatus = "error";
      }
    }
  }

  async authSignInRedirect(provider: "google" | "twitter" | "facebook") {
    if (provider === "google") {
      try {
        await this.authServ.googleLogin(false);
        this.errorStatus = "success";
      } catch (err) {
        this.errorStatus = "error";
        alert(err);
      }
    } else {
      try {
        await this.authServ.facebookLogin(false);
        this.errorStatus = "success";
      } catch (err) {
        this.errorStatus = "error";
      }
    }
  }

  //-------ROUTER ANIMS------
  getDepth(outlet: RouterOutlet) {
    let name = outlet.activatedRouteData["name"];
    if (name) return name;
    else return -1;
  }
}
