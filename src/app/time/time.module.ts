import { PaletteModule } from "./../shared/palette/palette.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TimeComponent } from "./time.component";
import { RouterModule, Routes } from "@angular/router";
import { ColorListComponent } from "./color-list/color-list.component";
import { PostersComponent } from "./posters/posters.component";
import { TimelineComponent } from "./timeline/timeline.component";

export const ROUTES: Routes = [
  {
    path: ":id",
    component: TimeComponent
  },
  { path: "**", redirectTo: "/time/today" }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), PaletteModule],
  declarations: [
    TimeComponent,
    ColorListComponent,
    PostersComponent,
    TimelineComponent
  ]
})
export class TimeModule {}
