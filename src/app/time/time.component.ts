import { AuthService } from "./../shared/auth/auth.service";
import { Palette, User } from "interfaces";
import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  ElementRef
} from "@angular/core";
import {
  trigger,
  style,
  state,
  transition,
  animate,
  AnimationEvent
} from "@angular/animations";
import { BehaviorSubject, Subscription } from "rxjs";
import { Store } from "../store";
import { ActivatedRoute, Router } from "@angular/router";
import { filter, map } from "rxjs/operators";
import { Poster } from "../../../interfaces";

@Component({
  selector: "time",
  templateUrl: "time.component.html",
  styleUrls: ["./time.component.sass"],
  animations: [
    trigger("fade", [
      state("fadeOut", style({ opacity: 0 })),
      state("fadeIn", style({ opacity: 1 })),

      transition(":enter", [style({ opacity: 0 }), animate("500ms ease")]),
      transition("fadeIn => fadeOut", animate("500ms ease")),
      transition("fadeOut => fadeIn", animate("500ms ease"))
    ])
  ]
})
export class TimeComponent implements OnInit {
  @ViewChild("right")
  right: ElementRef;

  postersToggle: boolean = true;
  fadeState: "fadeIn" | "fadeOut" = "fadeIn";
  timeLineState$: BehaviorSubject<any> = new BehaviorSubject({ val: "still" });
  animating: boolean = false;
  dates: string[];
  currDate: string;
  currDateIndex: number;
  palettes: Palette[];
  posters: Poster[];
  likes: string[] = [];
  text: string;
  sub: Subscription;
  constructor(
    private cd: ChangeDetectorRef,
    private route: ActivatedRoute,
    private store: Store,
    private authServ: AuthService,
    private router: Router
  ) {}

  //------LIFE CYCLE---------
  ngOnInit() {
    this.currDate = this.route.snapshot.params["id"];
    this.currDateIndex = CDDATES.indexOf(this.currDate);
    const date = CDDATA[this.currDate];
    if (date) {
      this.palettes = date["palettes"];
      this.posters = date["posters"];
      this.text = CDTEXT[this.currDate];
    } else {
      this.palettes = [];
      this.posters = [];
    }

    this.sub = this.store
      .select<User>("user")
      .pipe(
        filter(Boolean),
        map(user => user?.likes)
      )
      .subscribe(val => {
        if (val) {
          this.likes = [...val];
        }
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  //------METHODS---------

  rightToggle() {
    //switch
    this.postersToggle = !this.postersToggle;
    //scrollup
    (this.right.nativeElement as HTMLDivElement).scrollTop = 0;
  }

  changeDate(dir: "up" | "down") {
    if (this.animating) return;

    if (dir === "up") {
      //change date index up
      this.currDateIndex++;
      if (this.currDateIndex >= CDDATES.length) this.currDateIndex = 0;

      //animate
      this.timeLineState$.next({ val: "up" });
    } else {
      //change date index down
      this.currDateIndex--;
      if (this.currDateIndex < 0) this.currDateIndex = CDDATES.length - 1;

      //animate
      this.timeLineState$.next({ val: "down" });
    }

    //set animate
    this.fadeState = "fadeOut";
    this.animating = true;
    //change route
    this.router.navigate([`../${CDDATES[this.currDateIndex]}`], {
      relativeTo: this.route
    });
  }

  updatePalette(id: string) {
    //if logged in
    if (this.authServ.user) {
      //dislike
      let index = this.likes.indexOf(id);
      if (index !== -1) {
        this.likes.splice(index, 1);
        this.authServ.updatePalette([...this.likes]);
      }
      //like
      else {
        this.authServ.updatePalette([...this.likes, id]);
      }
    }
    //open modal if not logged in
    else {
      this.authServ.openModal$.emit();
    }
  }

  // ----------SET DATA AFTER FADEOUT CALLBACK----------
  fadeDone(e: AnimationEvent) {
    if (e.fromState !== "void") {
      if (this.fadeState === "fadeOut") {
        setTimeout(() => {
          //change data
          this.currDate = CDDATES[this.currDateIndex];
          const date = CDDATA[this.currDate];
          if (date) {
            this.palettes = date["palettes"];
            this.posters = date["posters"];
            this.text = CDTEXT[this.currDate];
          } else {
            this.palettes = [];
            this.posters = [];
          }
          //animation state
          this.fadeState = "fadeIn";

          //scrollup
          (this.right.nativeElement as HTMLDivElement).scrollTop = 0;

          //detect changes
          if (!this.cd["destroyed"]) {
            this.cd.detectChanges();
          }
        }, 0);
      } else if (this.fadeState === "fadeIn") {
        //delay being able to press button
        setTimeout(() => {
          this.animating = false;
          if (!this.cd["destroyed"]) {
            this.cd.detectChanges();
          }
        }, 100);
      }
    }
  }
}
