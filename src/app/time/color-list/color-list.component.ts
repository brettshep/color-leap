import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";
import { Palette } from "interfaces";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "color-list",
  template: `
    <div class="wrapper">
      <h1>Click a Color to Copy <i class="fas fa-clone"></i></h1>
      <!-- <div class="ad">
        <a
          target="_blank"
          href="https://www.youtube.com/watch?v=M3uBOwKTF0U"
          class="colors-ad"
        >
          <img src="assets/ad2.png" alt="youtube channel" />
        </a>
      </div> -->
      <div class="adobe-banner">
        <button (click)="openAdobe()">
          <img
            class="outer"
            src="/assets/adobeBannerMobile.png"
            width="300"
            height="250"
            border="0"
          />
          <div class="inner">
            <img
              src="/assets/adobeBannerInner.png"
              width="1007"
              height="60"
              border="0"
            />
            <img
              src="/assets/adobeBannerInner.png"
              width="1007"
              height="60"
              border="0"
            />
          </div>
        </button>
      </div>

      <ng-container *ngFor="let pal of palettes; index as i">
        <palette
          class="palette"
          [palette]="pal"
          [active]="likes?.includes(pal.id)"
          (update)="update(pal.id)"
        ></palette>
      </ng-container>
    </div>
  `,
  styleUrls: ["./color-list.component.sass"],
})
export class ColorListComponent {
  likes: string[];

  openAdobe() {
    //open link in new tab
    window.open("https://bit.ly/AdobeStockTrial");
  }

  @Input()
  set setLikes(val) {
    this.likes = val;
  }

  @Input()
  palettes: Palette[];

  @Input()
  date: string;

  @Output()
  updatePalette = new EventEmitter<string>();

  update(id: string) {
    this.updatePalette.emit(id);
  }
}
