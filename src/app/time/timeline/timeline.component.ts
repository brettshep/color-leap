import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Input,
  ViewChild,
  ElementRef
} from "@angular/core";
import {
  trigger,
  style,
  transition,
  animate,
  AnimationEvent,
  query,
  group
} from "@angular/animations";
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "timeline",
  template: `
<div class="timeLine" [@timeline]="timeLineState" (@timeline.done)="timeLineDone($event)">
  <div class="badge">
    <img #badgeImg [src]="'/assets/badges/'+date+'.png'" alt="badge" (load)="imgLoaded()">
  </div>
  <div class="boxShadow box1"></div>
  <div class="overflowSVG">
    <img class="img v" src="/assets/timeLineV.svg" alt="timeline">
    <img class="img v" src="/assets/timeLineV.svg" alt="timeline">
    <img class="img h" src="/assets/timeLineH.svg" alt="timeline">
    <img class="img h" src="/assets/timeLineH.svg" alt="timeline">
  </div>
  <div class="boxShadow box2"></div>
</div>
  `,
  styleUrls: ["./timeline.component.sass"],
  animations: [
    trigger("timeline", [
      transition(":enter", [
        group([
          query(".img", style({ opacity: 0 })),
          query(".img", animate("1000ms ease", style({ opacity: 0.2 })))
        ])
      ]),
      transition("still => down", [
        query(".v", style({ transform: "translateY(0%)" })),
        query(".h", style({ transform: "translateX(-100%)" })),
        group([
          query(
            ".v",
            animate("1000ms ease", style({ transform: "translateY(-100%)" }))
          ),
          query(
            ".h",
            animate("1000ms ease", style({ transform: "translateX(0%)" }))
          )
        ])
      ]),
      transition("still => up", [
        query(".v", style({ transform: "translateY(-100%)" })),
        query(".h", style({ transform: "translateX(0%)" })),
        group([
          query(
            ".v",
            animate("1000ms ease", style({ transform: "translateY(0%)" }))
          ),
          query(
            ".h",
            animate("1000ms ease", style({ transform: "translateX(-100%)" }))
          )
        ])
      ])
    ])
  ]
})
export class TimelineComponent {
  @ViewChild("badgeImg")
  badgeElem: ElementRef;
  @Input()
  set triggerAnim(state) {
    if (state.val) {
      this.timeLineState = state.val;
      if (this.badgeElem.nativeElement) {
        (this.badgeElem.nativeElement as HTMLImageElement).classList.add(
          "hidden"
        );
      }
    }
  }
  @Input()
  date: string;
  timeLineState: "up" | "down" | "still" = "still";

  constructor(private cd: ChangeDetectorRef) {}

  imgLoaded() {
    (this.badgeElem.nativeElement as HTMLImageElement).classList.remove(
      "hidden"
    );
  }

  //time line anim callback
  timeLineDone(e: AnimationEvent) {
    setTimeout(() => {
      //change data
      this.timeLineState = "still";
      if (!this.cd["destroyed"]) {
        this.cd.detectChanges();
      }
    }, 0);
  }
}
