import { Poster } from "./../../../../interfaces";
import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { CopyService } from "../../shared/copy.service";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "posters",
  template: `
    <div class="wrapper">
      <div 
      class="posterSect" 
      *ngFor="let poster of posters" 
      [style.background]="poster.bgCol">
        <div 
        class="bg" 
        [style.backgroundImage]="'url('+poster.image+')'">
        </div>
        
        <div class="imageCont">
          <div class="image">
            <img
            [src]="poster.image"
            >
          </div>
        </div>

        <div class="palette">
          <div class="overflow">
            <button 
            class="color" 
            *ngFor="let color of poster.colors"  
            (click)="copy(color)"
            [style.background]="color">
            </button>
          </div>
        </div>
      </div>

    </div>
  `,
  styleUrls: ["./posters.component.sass"]
})
export class PostersComponent {
  constructor(private copyServ: CopyService) {}

  @Input()
  posters: Poster[];

  copy(e: string) {
    this.copyServ.copy(e);
  }
}
