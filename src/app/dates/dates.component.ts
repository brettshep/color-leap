import {
  Component,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from "@angular/core";
import { FadeOut } from "../shared/animations";
import { AnimationEvent } from "@angular/animations";
import { Badge } from "../../../interfaces";

@Component({
  selector: "dates",
  template: `
    <div class="wrapper">
      <div class="logo">
        <a routerLink="/home">
          <img src="/assets/Logo.png" alt="logo" />
        </a>
      </div>
      <div class="grid">
        <a [routerLink]="'/time/' + badge.date" *ngFor="let badge of badges">
          <figure>
            <img [src]="badge.src" alt="badge" />
            <figcaption>
              {{ badge.date }}
            </figcaption>
          </figure>
        </a>
      </div>
      <!-- <div class="subscribe">
      <div class="content" [@FadeOut]="animState" (@FadeOut.done)="fadeDone($event)">
        <h1>Get updated on more awesome tools 🛠️ as we release them!</h1>
        <form  (submit)="subscribe($event)">
          <input type="text" placeholder="Email" #email (input)="onInput()">
          <button type="submit">GET FREE UPDATES</button>
        </form>
        <div  
        class="error" 
        *ngIf="emailError && submitted">
          <i class="fas fa-exclamation-triangle"></i>
          Please Enter A Valid Email
        </div>
      </div>
      <div 
      class="success" 
      *ngIf="showCheck">
        <i class="fas fa-check-circle"></i>
      </div>
    </div> -->
      <div class="adobe-banner">
        <button (click)="openAdobe()">
          <img
            class="outer"
            src="/assets/adobeBanner.png"
            width="728"
            height="90"
            border="0"
          />
          <div class="inner">
            <img
              src="/assets/adobeBannerInner.png"
              width="1007"
              height="60"
              border="0"
            />
            <img
              src="/assets/adobeBannerInner.png"
              width="1007"
              height="60"
              border="0"
            />
          </div>
        </button>
      </div>
      <div class="adobe-banner-mobile">
        <button (click)="openAdobe()">
          <img
            class="outer"
            src="/assets/adobeBannerMobile.png"
            width="300"
            height="250"
            border="0"
          />
          <div class="inner">
            <img
              src="/assets/adobeBannerInner.png"
              width="1007"
              height="60"
              border="0"
            />
            <img
              src="/assets/adobeBannerInner.png"
              width="1007"
              height="60"
              border="0"
            />
          </div>
        </button>
      </div>
      <div class="socialCont">
        <h3>SHARE ON</h3>
        <div class="socialBtnCont">
          <a
            target="_blank"
            href="https://www.facebook.com/sharer/sharer.php?u=https%3A//colorleap.app"
          >
            <div id="facebook" class="share">
              <i class="fab fa-facebook-f"></i>
            </div>
          </a>
          <a
            target="_blank"
            href="https://twitter.com/home?status=https%3A//colorleap.app"
          >
            <div id="twitter" class="share">
              <i class="fab fa-twitter"></i>
            </div>
          </a>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./dates.component.sass"],
  animations: [FadeOut],
})
export class DatesComponent {
  @ViewChild("email")
  emailElem: ElementRef;

  badges: Badge[];
  regex: RegExp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  emailError: boolean = true;
  submitted: boolean = false;
  showCheck: boolean = false;
  animState: "visible" | "notVisible" = "visible";

  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.badges = CDBADGES;
  }

  openAdobe() {
    //open link in new tab
    window.open("https://bit.ly/AdobeStockTrial");
  }

  onInput() {
    const elem: HTMLInputElement = this.emailElem.nativeElement;
    if (this.regex.test(elem.value)) {
      this.emailError = false;
    } else {
      this.emailError = true;
    }
  }

  fadeDone(e: AnimationEvent) {
    if (e.fromState === "visible") {
      setTimeout(() => {
        //change data
        this.showCheck = true;
        //detect changes
        if (!this.cd["destroyed"]) {
          this.cd.detectChanges();
        }
      }, 0);
    }
  }
}
