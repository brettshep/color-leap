import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DatesComponent } from "./dates.component";

import { RouterModule, Routes } from "@angular/router";

export const ROUTES: Routes = [
  {
    path: "",
    component: DatesComponent
  },
  { path: "**", redirectTo: "/dates" }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  declarations: [DatesComponent]
})
export class DatesModule {}
