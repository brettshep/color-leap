import { Injectable, EventEmitter } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class CopyService {
  copied$ = new EventEmitter();

  copy(text: string) {
    //create elem and copy
    let textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.style.position = "fixed";
    textArea.style.background = "transparent";
    textArea.style.top = "0";
    textArea.style.left = "0";

    if (this.isIOS) {
      textArea.contentEditable = "true";
      textArea.readOnly = true;

      // create a selectable range
      var range = document.createRange();
      range.selectNodeContents(textArea);

      // select the range
      var selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
      textArea.setSelectionRange(0, 999999);
    } else {
      textArea.focus();
      textArea.select();
    }

    document.execCommand("copy");
    document.body.removeChild(textArea);
    this.copied$.emit();
  }

  get isIOS(): boolean {
    return !!navigator.userAgent.match(/ipad|ipod|iphone/i);
  }
}
