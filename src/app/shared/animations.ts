import {
  trigger,
  transition,
  style,
  animate,
  state
} from "@angular/animations";
export const FadeInOut = trigger("FadeInOut", [
  transition(":enter", [
    style({
      opacity: 0,
      zIndex: 1000,
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
    }),
    animate(
      ".3s ease",
      style({
        opacity: 1
      })
    )
  ]),
  transition(":leave", [
    style({
      opacity: 1,
      zIndex: 1000,
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
    }),
    animate(
      ".3s ease",
      style({
        opacity: 0
      })
    )
  ])
]);

export const FadeOut = trigger("FadeOut", [
  state(
    "notVisible",
    style({
      opacity: 0
    })
  ),
  transition("visible => notVisible", [
    style({
      opacity: 1
    }),
    animate(
      ".5s ease",
      style({
        opacity: 0
      })
    )
  ])
]);
