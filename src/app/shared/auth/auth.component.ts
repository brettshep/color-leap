import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  Input
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "auth",
  template: `
    <div class="wrapper" (click)="bgClick($event)">
      <div class="modal">
        <div class="exit" (click)="exit.emit()"><i class="fas fa-times"></i></div>
        
        <h1>SIGN IN</h1>

        <h2>Save all your <i class="fas fa-heart"></i> palettes!</h2>
        
        <div class="buttons">
          <button id="google" (click)="signInEmit('google')">
          <span>CONTINUE WITH <i class="fab fa-google"></i></span>
          </button>
          
          <button id="facebook" (click)="signInEmit('facebook')">
            <span>CONTINUE WITH <i class="fab fa-facebook-square"></i></span>
          </button> 
        </div>

        <p>By doing so, you agree to our <a routerLink="/privacy-policy" (click)="exit.emit()">Privacy Policy</a> and <a routerLink="/terms" (click)="exit.emit()">Terms of Service</a>.</p>

        <div id="error" *ngIf="error"><i class="fas fa-exclamation-triangle"></i>Something went wrong...</div>

      </div>
    </div>
  `,
  styleUrls: ["./auth.component.sass"]
})
export class AuthComponent {
  error = false;
  isMobile: boolean;

  @Input()
  set errorStatus(e: "error" | "success") {
    if (e === "error") {
      this.error = true;
    } else if (e === "success") {
      this.exit.emit();
    }
  }
  @Output()
  exit = new EventEmitter();

  @Output()
  signIn = new EventEmitter<{
    provider: "google" | "twitter" | "facebook";
    popUp: boolean;
  }>();

  ngOnInit() {
    let ua = navigator.userAgent;
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(
        ua
      )
    ) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
  }

  signInEmit(e: "google" | "twitter" | "facebook") {
    this.signIn.emit({ provider: e, popUp: !this.isMobile });
  }

  bgClick(e: MouseEvent) {
    if ((e.target as HTMLElement).classList.contains("wrapper")) {
      this.exit.emit();
    }
  }
}
