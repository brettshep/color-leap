import { Injectable, EventEmitter, inject } from "@angular/core";
import { of } from "rxjs";
import { tap, switchMap, map } from "rxjs/operators";
import { Store } from "../../store";
import { User } from "interfaces";
import { Auth, FacebookAuthProvider, getAdditionalUserInfo, getRedirectResult, GoogleAuthProvider, signInWithPopup, signInWithRedirect, signOut, TwitterAuthProvider, user } from "@angular/fire/auth";
import { collection, doc, docSnapshots, Firestore, setDoc, updateDoc } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(
    private store: Store,
  ) {}

  afAuth: Auth = inject(Auth);
  fireStore: Firestore = inject(Firestore);

  authUser$ = user(this.afAuth);

  usersColl = collection(this.fireStore, "users")


  //------OPEN MODAL----------
  openModal$ = new EventEmitter();
  showSpinner$ = new EventEmitter<boolean>();

  //------USER OBSERVALBE----------
  user$ = this.authUser$.pipe(
    switchMap((user) => {
      if (user) { 
        return docSnapshots<User>(doc(this.fireStore, "users",user.uid) as any).pipe(map((doc) => doc.data()));
      } else {
        return of(null);
      }
    }),
    tap((user) => {
      this.store.set("user", user);
    })
  );

  //------------OAUTH PRIVIDERS-----------
  googleLogin(popup: boolean): Promise<void> {
    const provider = new GoogleAuthProvider();
    if (popup) return this.oAuthLoginPopUp(provider);
    else return this.oAuthLoginRedirect(provider);
  }

  twitterLogin(popup: boolean): Promise<void> {
    const provider = new TwitterAuthProvider();
    if (popup) return this.oAuthLoginPopUp(provider);
    else return this.oAuthLoginRedirect(provider);
  }

  facebookLogin(popup: boolean): Promise<void> {
    const provider = new FacebookAuthProvider();
    if (popup) return this.oAuthLoginPopUp(provider);
    else return this.oAuthLoginRedirect(provider);
  }

  private oAuthLoginPopUp(provider: any): Promise<void> {
    return signInWithPopup(this.afAuth, provider).then((credential) => {
      //set default data if new user  
       const details = getAdditionalUserInfo(credential)
      if (details.isNewUser) {
        this.setUserData(credential.user);
      }
    });
  }

  private oAuthLoginRedirect(provider: any): Promise<void> {
    window.sessionStorage.setItem("pending", "true");
    return signInWithRedirect(this.afAuth, provider);
  }

  //set user info in Firestore
  private setUserData(user: any) {
    const userRef = doc(this.fireStore, "users", user.uid)

    let data: User = {
      uid: user.uid,
      email: user.email,
      likes: [],
    };


    return setDoc(userRef, data, { merge: true })
  }

  //--------LOGOUT---------
  logOutUser(): Promise<void> {
    return signOut(this.afAuth);
  }

  //----------Likes-------------
  updatePalette(likes) {
    updateDoc(doc(this.fireStore, "users", this.UID), { likes });
  }

  //----STUPID REDIRECT SPINNER----

  checkRedirect() {
    //----SPINNER FOR REDIRECT-----
    if (window.sessionStorage.getItem("pending")) {
      window.sessionStorage.removeItem("pending");
      this.showSpinner$.emit(true);
    }
      getRedirectResult(this.afAuth)
      .then((credential) => {
        this.showSpinner$.emit(false);
        const details = getAdditionalUserInfo(credential)
        //set default data if new user
        if (
          details?.isNewUser
        ) {
          this.setUserData(credential.user);
        }
      })
      .catch(() => {
        this.showSpinner$.emit(false);
      });
  }

  //----------UTILS-----------
  get UID() {
    return this.afAuth.currentUser.uid;
  }

  get user() {
    return this.afAuth.currentUser;
  }
}
