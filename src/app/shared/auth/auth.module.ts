import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthComponent } from "./auth.component";
RouterModule;
@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [AuthComponent],
  exports: [AuthComponent]
})
export class AuthModule {}
