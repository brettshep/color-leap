import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PaletteComponent } from "./palette.component";

@NgModule({
  imports: [CommonModule],
  declarations: [PaletteComponent],
  exports: [PaletteComponent]
})
export class PaletteModule {}
