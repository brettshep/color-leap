import { CopyService } from "./../copy.service";
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Palette } from "../../../../interfaces";

@Component({
  selector: "palette",
  template: `
<div class="wrapper">
  <div class="colors">
    <div 
    class="color"
    *ngFor="let color of palette.colors"
    (click)="copy(color)"
    [style.background]="color">
    </div>
  </div>
  <h2 [class.active]="active">
  {{palette.name}}
    <i class="fas fa-heart" (click)="update.emit()"></i>
  </h2>
</div>
  `,
  styleUrls: ["./palette.component.sass"]
})
export class PaletteComponent {
  constructor(private copyServ: CopyService) {}
  @Input()
  palette: Palette;
  @Input()
  active: boolean;
  @Output()
  update = new EventEmitter();

  copy(e: string) {
    this.copyServ.copy(e);
  }
}
