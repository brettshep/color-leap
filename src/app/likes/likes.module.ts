import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { LikesComponent } from "./likes.component";
import { PaletteModule } from "../shared/palette/palette.module";
import { ReversePipe } from './reverse.pipe';

export const ROUTES: Routes = [
  {
    path: "",
    component: LikesComponent
  },
  { path: "**", redirectTo: "/likes" }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), PaletteModule],
  declarations: [LikesComponent, ReversePipe]
})
export class LikesModule {}
