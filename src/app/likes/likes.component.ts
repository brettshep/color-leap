import { User } from "./../../../interfaces";
import { Component, OnInit } from "@angular/core";
import { AuthService } from "../shared/auth/auth.service";
import { Subscription } from "rxjs";
import { Store } from "../store";
import { filter, map } from "rxjs/operators";
import { Palette } from "../../../interfaces";
import { Router } from "@angular/router";

@Component({
  selector: "likes",
  template: `
  <div class="wrapper">
    <div class="logo">
      <a routerLink="/home">
        <img src="/assets/Logo.png" alt="logo">
      </a>
    </div>
    <div class="grid">
      <ng-container *ngFor="let pal of palettes | reverse; index as i">
        <palette
        class="palette"
        [palette]="pal"
        [active]="true"
        (update)="updatePalette(pal.id)"
        ></palette>
      </ng-container>
    </div>
    <button (click)="logout()">
      LOGOUT
    </button>
  </div>
  `,
  styleUrls: ["./likes.component.sass"]
})
export class LikesComponent implements OnInit {
  likes: string[] = [];
  sub: Subscription;
  palettes: Palette[] = [];

  constructor(
    private authServ: AuthService,
    private store: Store,
    private router: Router
  ) {}

  //---------LIFECYCLE HOOKS--------

  ngOnInit() {
    this.sub = this.store
      .select<User>("user")
      .pipe(
        filter(Boolean),
        map((user) => user.likes)
      )
      .subscribe(val => {
        if (val) {
          this.likes = [...val];
          this.setPalettes();
        }
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  //--------METHODS--------

  setPalettes() {
    this.palettes = this.likes.map(like => {
      let arr = like.split("-");
      return CDDATA[arr[0]]["palettes"][arr[1]];
    });
  }

  updatePalette(id: string) {
    //if logged in
    if (this.authServ.user) {
      //dislike
      let index = this.likes.indexOf(id);
      this.likes.splice(index, 1);
      this.authServ.updatePalette([...this.likes]);
    }
    //open modal if not logged in
    else {
      this.authServ.openModal$.emit();
    }
  }

  logout() {
    this.authServ.logOutUser();
    this.router.navigateByUrl("/home");
  }
}
