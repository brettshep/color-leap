import { AuthGuard } from "./shared/auth/authGuard.guard";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

export enum CompName {
  home = 1,
  dates,
  time,
  likes
}

const routes: Routes = [
  {
    path: "home",
    data: { name: CompName.home },
    loadChildren: () => import("./home/home.module").then(m => m.HomeModule)
  },
  {
    path: "dates",
    data: { name: CompName.dates },
    loadChildren: () => import("./dates/dates.module").then(m => m.DatesModule)
  },
  {
    path: "time",
    data: { name: CompName.time },
    loadChildren: () => import("./time/time.module").then(m => m.TimeModule)
  },
  {
    path: "likes",
    data: { name: CompName.likes },
    loadChildren: () => import("./likes/likes.module").then(m => m.LikesModule),
    canActivate: [AuthGuard]
  },
  {
    path: "terms",
    loadChildren:
      () => import("./terms-conditions/terms-conditions.module").then(m => m.TermsConditionsModule)
  },
  {
    path: "privacy-policy",
    loadChildren: () => import("./privacy-policy/privacy-policy.module").then(m => m.PrivacyPolicyModule)
  },
  { path: "**", redirectTo: "home", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class AppRoutingModule {}
